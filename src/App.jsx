import { useLocalStorage } from "@uidotdev/usehooks";
import { useEffect, useState } from "react";
import axios from "axios";
import {Form, Button, FormGroup, Input, Container, Row, Col, Card, CardHeader, CardBody, CardFooter, CardTitle, CardText} from "reactstrap";
import './App.css'

function App() {
  const [id] = useLocalStorage("id", Math.floor(Math.random() * 10000000000));
  const [notes, setNotes] = useState([]);
  const [state, setState] = useState("connecting"); //connected | fail

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const _onSubmit = async (e) => {
    e.preventDefault();

    if (state === "fail") {
      setNotes([{uid: Math.floor(Math.random() * 100000000), title, content}, ...notes]);
    }

    if (state === "connected") {
      await _post(title, content)
    }

    setTitle("");
    setContent("");
  }
  const _onDelete = async (uid) => {
    if (state === "fail") {
      setNotes(notes.filter(i => i.uid !== uid))
      return
    }

    if (state === "connected") {
      await _delete(uid)
    }
  }

  const _refresh = async () => {
    const res = await axios.get("/notes")
    console.log(res.data);
    setNotes(res.data)
  }

  useEffect(() => {
    (async () => {
      try {
        const res = await axios.get("/healthcheck")
        if (res.status === 200) {
          axios.defaults.headers.common["userId"] = id;
          setState("connected");
        } else {
          setState("fail")
        }
      } catch (e) {
        setState("fail")
      }
    })()
  }, [])

  useEffect(() => {
    if (state !== "connected") return

    (async () => {
      try { 
        await _refresh()
      } catch (e) {
        setState("fail")
      }
    })()
  }, [state])

  const _post = async (title, content) => {
    await axios.post("/notes", {title, content})
    await _refresh()
  }

  const _delete = async (uid) => {
    await axios.delete(`/notes/${uid}`)
    await _refresh()
  }

  const _update = async (uid, title, content) => {
    await axios.put(`/notes/${uid}`, {title, content})
    await _refresh()
  }

  return (
    <>
      <Container className="w-100">
        <Row className="mb-4">
          <Col sm={3}></Col>
          <Col sm={3} xs={12}>
            <Card>
              <CardTitle className="text-center">ID</CardTitle>
              <CardText className="text-center">{id}</CardText>
            </Card>
          </Col>
          <Col sm={3} xs={12}>
            <Card>
              <CardTitle className="text-center">Status API</CardTitle>
              <CardText className="text-center" style={{
                backgroundColor: state === "connected" ? "green" : 
                                 state === "connecting" ? "orange" : "red",
                color: "white"
              }}>{state}</CardText>
            </Card>
          </Col>
          <Col sm={3}></Col>
        </Row>
        <Row>
          <Col lg={2}></Col>
          <Form className="text-center border p-4 rounded-2 col-lg-8" onSubmit={_onSubmit}>
            <FormGroup>
              <Input placeholder="Titre..." onChange={e => setTitle(e.target.value)} value={title} />
            </FormGroup>
            <FormGroup>
              <Input placeholder="Contenu...." type="textarea" onChange={e => setContent(e.target.value)} value={content} />
            </FormGroup>
            <Button>Ajouter</Button>
          </Form>
          <Col lg={2}></Col>
        </Row>

        <Row>
          {notes.length === 0 ? 
            <Col lg={12} className="text-center mt-4">
              <h3>Aucune note ...</h3>
            </Col> : null
          }

          {notes.map(i => (
            <Col lg={3} md={4} sm={6} xs={12} key={i.uid} className="mt-4 text-left">
              <Card className="relative">
                <CardBody>
                  <h3 className="text-left">{i.title}</h3>
                  <p>{i.content}</p>
                </CardBody>
                <Button  color="danger" onClick={() => _onDelete(i.uid)}>X</Button>
              </Card>
            </Col>
          ))}

        </Row>
      </Container>
    </>
  )
}

export default App
